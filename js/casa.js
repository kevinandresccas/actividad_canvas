const v1 = document.getElementById("casa");
console.log(v1);
const context = v1.getContext("2d");

context.lineWidth = 6

//Zona izquierda de la casa
context.moveTo(40,215);
context.lineTo(40,450);
context.lineTo(250,450);

//Zona derecha de la casa
//context.fillStyle = "blue"
//context.strokeStyle = "green"
context.rect(250,210,400,240);

//Techo zona derecha
context.lineTo(670, 210);
context.lineTo(640,90);
context.lineTo(170,90)

//Techo zona Izquierda
context.moveTo(30,215)
context.lineTo(145,50)
context.lineTo(260,215)
context.lineTo(245,215)
context.lineTo(145,75)
context.lineTo(50,215)
context.lineTo(30,215)

context.stroke();
context.beginPath();

//Ventanas  1 - 2
//          3 - 4
// Ventana 1
context.rect(75,210,60,75)
context.moveTo(75,245)
context.lineTo(135,245)
//Ventana 2
context.rect(160,210,60,75)
context.moveTo(160,245)
context.lineTo(220,245)
//Ventana 3
context.rect(75,310,60,75)
context.moveTo(75,345)
context.lineTo(135,345)
//Ventana 4
context.rect(160,310,60,75)
context.moveTo(160,345)
context.lineTo(220,345)

//chimenea
context.rect(565,40,40,50)
context.rect(561,30,48,10)

//Ventanas zona derecha
//Ventana 1
context.rect(275,245,65,140)
context.moveTo(275,280)
context.lineTo(340,280)
context.moveTo(307.5,280)
context.lineTo(307.5,385)

context.moveTo(275,307)
context.lineTo(340,307)

context.moveTo(275,334)
context.lineTo(340,334)

context.moveTo(275,359)
context.lineTo(340,359)

//Ventana 2
context.rect(560,245,65,140)
context.moveTo(560,280)
context.lineTo(625,280)
context.moveTo(592.5,280)
context.lineTo(592.5,385)

context.moveTo(560,307)
context.lineTo(625,307)

context.moveTo(560,334)
context.lineTo(625,334)

context.moveTo(560,359)
context.lineTo(625,359)

//Puerta
context.rect(403,320,84,130)
context.rect(415,332,60,25)
context.rect(415,370,60,68)

//Escaleras
context.rect(365,450,160,15)
context.moveTo(364,465)
context.lineTo(361,480)
context.lineTo(528,480)
context.lineTo(525,465)

//Marco de puerta
context.rect(365,320,10,130)
context.rect(515,320,10,130)
context.moveTo(360,320)
context.stroke();
context.beginPath

context.arc(445,320, 85,Math.PI/180 * 180,2*Math.PI)
context.moveTo(360,320)
context.lineTo(380,320)
context.arc(445,320, 65,Math.PI/180 * 180,2*Math.PI)
context.stroke();

//Arboles
context.rect(750,350,40,120)
context.ellipse(770,320,30,45,1.6,0,2*Math.PI,true)

context.rect(810,380,30,90)
context.moveTo(840,385)
context.arc(825,360,20,0,2*Math.PI)

context.rect(860,410,30,60)
context.ellipse(875,396,12,15,1.6,0,2*Math.PI,true)

context.stroke();



